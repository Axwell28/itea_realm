//
//  StudentModel.swift
//  ITEA_Realm
//
//  Created by Alex Marfutin on 7/16/19.
//  Copyright © 2019 G9. All rights reserved.
//

import Foundation
import RealmSwift

class StudentModel: Object {
    
    @objc dynamic var name : String = ""
    @objc dynamic var surname : String = ""
    @objc dynamic var age : Int = 0
    @objc dynamic var sex : String = ""
    @objc dynamic var city : String = ""
}
