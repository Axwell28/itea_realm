//
//  StudentTableViewController.swift
//  ITEA_Realm
//
//  Created by Alex Marfutin on 7/16/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import RealmSwift

class StudentTableViewController: UIViewController {

    
    @IBOutlet var studentTable: UITableView!
    
    var students : Results<StudentModel>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        studentTable.register(UINib(nibName: "StudentsTableViewCell", bundle: nil), forCellReuseIdentifier: "StudentsTableViewCell")
        studentTable.delegate = self
        studentTable.dataSource = self
        students = try! Realm().objects(StudentModel.self).sorted(byKeyPath: "name")
    }
    override func viewDidAppear(_ animated: Bool) {
        studentTable.reloadData()
    }
    
}
extension StudentTableViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let student = students {
        return student.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentsTableViewCell") as! StudentsTableViewCell
        if let student = students {
            cell.nameLabel.text = student[indexPath.row].name
            cell.surnameLabel.text = student[indexPath.row].surname
            cell.ageLabel.text = String(student[indexPath.row].age)
            cell.sexLabel.text = student[indexPath.row].sex
            cell.adressLabel.text = student[indexPath.row].city
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
