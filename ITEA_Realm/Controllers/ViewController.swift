//
//  ViewController.swift
//  ITEA_Realm
//
//  Created by Alex Marfutin on 7/16/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import RealmSwift

class ViewController: UIViewController {

    
    @IBOutlet var allStudentButton: UIButton!
    @IBOutlet var clearButton: UIButton!
    @IBOutlet var addButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        allStudentButton.layer.cornerRadius = 25
        clearButton.layer.cornerRadius = 25
        addButton.layer.cornerRadius = 25
        
    }

    @IBAction func onAllStudentButtonTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "StudentTableViewController") as! StudentTableViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onClearButtonTapped(_ sender: Any) {
        let alert = UIAlertController(title: "Modify", message: "Do you realy want to delete student base?", preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler:  { action in let realm = try! Realm()
            realm.beginWrite()
            realm.deleteAll()
            try! realm.commitWrite()}))
       alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func onAddButtonTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddStudentViewController") as! AddStudentViewController
        navigationController?.pushViewController(vc, animated: true)
    }
}
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
