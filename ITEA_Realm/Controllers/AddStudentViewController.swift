//
//  AddStudentViewController.swift
//  ITEA_Realm
//
//  Created by Alex Marfutin on 7/16/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit
import RealmSwift

class AddStudentViewController: UIViewController {
    
    @IBOutlet var nameText: UITextField!
    @IBOutlet var surnameText: UITextField!
    @IBOutlet var ageText: UITextField!
    @IBOutlet var sexCheck: UIPickerView!
    @IBOutlet var adressText: UITextField!
    @IBOutlet var addStudentButton: UIButton!
    
    var pickerData: [String] = ["Male","Famale","Another"]
    var rowSelected = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        addStudentButton.layer.cornerRadius = 25
        self.hideKeyboardWhenTappedAround()
        sexCheck.delegate = self
        sexCheck.dataSource = self
    }
    
    @IBAction func onAddStudentButtonTapped(_ sender: Any) {
        let student = StudentModel()
        let realm = try! Realm()
        realm.beginWrite()
        if let name = nameText.text {
            student.name = name
        } else {student.name = ""}
        
        if let surname = surnameText.text {
            student.surname = surname
        } else {student.surname = ""}
        
        if let age = ageText.text {
            student.age = Int(age) ?? 0
        } else {student.age = 1}
        
        if sexCheck != nil {
            student.sex = pickerData[rowSelected]
        } else {student.sex = ""}
        
        if let adress = adressText.text {
            student.city = adress
        } else {student.city = ""}
        realm.create(StudentModel.self, value: student)
        try! realm.commitWrite()
        navigationController?.popViewController(animated: true)
    }
}

extension AddStudentViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.rowSelected = row
    }
    
}
