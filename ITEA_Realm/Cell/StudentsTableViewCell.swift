//
//  StudentsTableViewCell.swift
//  ITEA_Realm
//
//  Created by Alex Marfutin on 7/16/19.
//  Copyright © 2019 G9. All rights reserved.
//

import UIKit

class StudentsTableViewCell: UITableViewCell {

    
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var surnameLabel: UILabel!
    @IBOutlet var ageLabel: UILabel!
    @IBOutlet var sexLabel: UILabel!
    @IBOutlet var adressLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
